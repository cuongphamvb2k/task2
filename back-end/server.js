const express = require('express')
var cors = require('cors')
const app = express()
const port = 8000

app.use(cors())

app.get('/device_summary', (req, res) => {
  setTimeout(() => { res.send(JSON.stringify({ ios: 60, android: 40 })) }, 2000)
})
app.get('/ranking_data', (req, res) => {
  setTimeout(() => {
    res.send(JSON.stringify([
      {
        day: 'Day 1',
        number: 12
      }, {
        day: 'Day 2',
        number: 5
      },
      {
        day: 'Day 3',
        number: 5
      },
      {
        day: 'Day 4',
        number: 4
      },
      {
        day: 'Day 5',
        number: 4
      },
      {
        day: 'Day 6',
        number: 4
      },
      {
        day: 'Day 7',
        number: 2
      },
      {
        day: 'Day 8',
        number: 7
      },
      {
        day: 'Day 9',
        number: 3
      }
    ]))
  }, 15000)
})



const arrDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
const arrHours = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
app.get('/device_by_hour', (req, res) => {
  setTimeout(() => {
    res.send(JSON.stringify(

      arrDays.map((day) => ({
        day: day,
        hours: arrHours.map(time => ({
          time: `${time}:00`,
          value: Math.floor(Math.random() * 2.5) !== 2 ? Math.floor(Math.random() * 30, 5) : Math.floor(Math.random() * 50.5)
        }))
      })
      )


    ))
  }, 5000)
})
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

