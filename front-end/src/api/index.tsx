import axios from 'axios';
const URL = 'https://blog-server-bnk.herokuapp.com/api';
export const fetchPieChartData = () => axios.get(`${URL}/device`);
export const fetchRankingChartData = () => axios.get(`${URL}/ranking_data`);
export const fetchHeatMapChartData = () => axios.get(`${URL}/device_by_hour`);
