import React from 'react';

import styles from '../PieChart/PieChart.module.scss';
function LoadingPieChart() {
    return (
        <div className={styles['loading']}>
            <div className="spinner-border text-primary" role="status"></div>
        </div>
    );
}

export default LoadingPieChart;
