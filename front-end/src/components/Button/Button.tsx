import './button.css';

import React from 'react';
interface ButtonProps {
    backgroundColor?: string;
    label: string;
    onClick?: () => void;
    type?: 'Cancel' | 'Ok' | undefined;
}
export const Button = ({ backgroundColor, label, type, ...props }: ButtonProps) => {
    const mode = type === 'Ok' ? 'storybook-button--primary' : 'storybook-button--secondary';
    return (
        <a type={type} className={['storybook-button', mode].join(' ')} style={{ backgroundColor }} {...props}>
            {label}
        </a>
    );
};