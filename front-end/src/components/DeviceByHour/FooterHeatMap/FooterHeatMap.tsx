import React from 'react';

import styles from './FooterHeatMap.module.scss';

function FooterHeatMap() {
    return ( 
        <div className={styles['footer-heatmap']}>
        <div className={styles['footer-hours']}>
            <div className={styles['hours']}>
                <div>0</div>
                <div>am</div>
            </div>
            <div className={styles['hours']}></div>
            <div className={styles['hours']}>2</div>
            <div className={styles['hours']}></div>
            <div className={styles['hours']}>4</div>
            <div className={styles['hours']}></div>
            <div className={styles['hours']}>6</div>
            <div className={styles['hours']}></div>
            <div className={styles['hours']}>8</div>
            <div className={styles['hours']}></div>
            <div className={styles['hours']}>10</div>
            <div className={styles['hours']}></div>
            <div className={styles['hours']}>
                <div>12</div>
                <div>pm</div>
            </div>
            <div className={styles['hours']}></div>
            <div className={styles['hours']}>2</div>
            <div className={styles['hours']}></div>
            <div className={styles['hours']}>4</div>
            <div className={styles['hours']}></div>
            <div className={styles['hours']}>6</div>
            <div className={styles['hours']}></div>
            <div className={styles['hours']}>8</div>
            <div className={styles['hours']}></div>
            <div className={styles['hours']}>10</div>
            <div className={styles['hours']}></div>
        </div>
        <div className={styles['parameters']}>
            <div className={styles['parameters-bar']}></div>
            <div className={styles['parameters-number']}>
                <span>0</span>
                <span>5</span>
                <span>10</span>
                <span>15</span>
            </div>
        </div>
    </div>
     );
}

export default FooterHeatMap;