import React from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { heatMapChartState$ } from 'src/redux/selector';

import * as actions from '../../redux/actions';
import LoadingPieChart from '../Loading';
import styles from './HeatChart.module.scss';
import HeatMap from './HeatMap/HeatMap';
import ParameterHeatMap from './ParameterHeatMap/ParameterHeatMap';

function HeatChart() {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(actions.getHeatMapChartData());
    }, [dispatch]);
    const dataHeatMapState = useSelector(heatMapChartState$);
    return (
        <div className={styles['heat-chart']}>
            <div className={styles['title']}>Device by hour</div>
            {dataHeatMapState?.isLoading ? (
                <LoadingPieChart />
            ) : (
                <div className={styles['chart']}>
                    <HeatMap />
                    <ParameterHeatMap />
                </div>
            )}
            {!dataHeatMapState.isLoading ? (
                <div className={styles['notification']}>
                    {dataHeatMapState?.isFailure?.message ? dataHeatMapState?.isFailure?.message : ''}
                </div>
            ) : (
                ''
            )}
        </div>
    );
}
export default HeatChart;
