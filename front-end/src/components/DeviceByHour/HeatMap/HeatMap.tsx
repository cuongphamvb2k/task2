import React from 'react';
import { useSelector } from 'react-redux';
import { heatMapChartState$ } from 'src/redux/selector';
import { Hours } from 'src/type/chart-data.type';

import FooterHeatMap from '../FooterHeatMap/FooterHeatMap';
import styles from './HeatMap.module.scss';

function HeatMap() {
    const dataHeatMapState = useSelector(heatMapChartState$);
    const dataHeatMap = dataHeatMapState.data.data;
    return (
        <div className={styles['chart-heatmap']}>
            <div className={styles['row-heatmap']}>
                {dataHeatMap?.map((datas) =>
                    datas.hours.map((value: Hours, index: number) => (
                        <div
                            className={styles['element']}
                            key={index}
                            title={`${value.value}`}
                            style={{
                                backgroundColor: `rgba(140, 110, 219, ${(1 / 50) * value.value})`,
                            }}
                        ></div>
                    )),
                )}
            </div>
            <FooterHeatMap />
        </div>
    );
}

export default HeatMap;
