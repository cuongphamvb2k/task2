import React from 'react';
import { useSelector } from 'react-redux';
import { heatMapChartState$ } from 'src/redux/selector';
import { ArrTotalDay } from 'src/type/chart-data.type';

import styles from './ParameterHeatMap.module.scss';

function ParameterHeatMap() {
    const dataHeatMapState = useSelector(heatMapChartState$);
    const dataHeatMap = dataHeatMapState.data.data;

    const arrTotalDay: ArrTotalDay[] = [];
    const arrWeekdays: string[] = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    dataHeatMap?.map((data) => {
        let sum = 0;
        for (let i = 0; i <= arrWeekdays.length; i++) {
            if (data.day === arrWeekdays[i]) {
                data.hours.map((hours) => {
                    return (sum += hours.value);
                });
                arrTotalDay.push({ day: data.day, total: sum });
            }
        }
    });
    function MaxNumber() {
        const arr: number[] = [];
        arrTotalDay.map((value: any) => arr.push(value.total));
        return Math.max(...arr);
    }
    return (
        <>
            <div className={styles['days']}>
                {dataHeatMap?.map((data, index) => (
                    <div key={index} className={styles['day']}>
                        {data.day.slice(0, 3)}
                    </div>
                ))}
            </div>

            <div className={styles['bar-chart']}>
                {arrTotalDay.map((datas: any, index: any) => (
                    <div key={index} className={styles['total-day']}>
                        <div className={styles['percent']} style={{ width: `${(100 / MaxNumber()) * datas.total}%` }}>
                            <span>{datas.total}</span>
                        </div>
                    </div>
                ))}
                <div className={styles['total-number']}>
                    <span>0</span>
                    <span>{MaxNumber()}</span>
                </div>
            </div>
        </>
    );
}

export default ParameterHeatMap;
