import React from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import * as actions from '../../redux/actions';
import { rankingChartState$ } from '../../redux/selector';
import LoadingPieChart from '../Loading';
import styles from './RankingChart.module.scss';

function RankingChart() {
    const dispatch = useDispatch();
    const dataRankings = useSelector(rankingChartState$);
    useEffect(() => {
        dispatch(actions.getRankingChartData());
    }, [dispatch]);
    function MaxNumber() {
        const arr: number[] = [];
        dataRankings?.data?.data?.map((dataRanking) => arr.push(dataRanking.number));

        return Math.max(...arr);
    }

    return (
        <div className={styles['ranking-chart']}>
            <div className={styles['title']}>Ranking </div>
            {dataRankings.isLoading ? (
                <LoadingPieChart />
            ) : (
                <div className={styles['description']}>
                    <div className={styles['chart']}>
                        <div className={styles['chart-title']}>
                            <div className={styles['day']}>Day</div>
                            <div className={styles['number']}>Number</div>
                        </div>
                        <div className={styles['wrapper']}>
                            {dataRankings?.data?.data?.map((dataRanking, index) => (
                                <div key={index} className={styles['content']}>
                                    <div className={styles['information']}>
                                        <div className={styles['content-day']}>{dataRanking.day}</div>
                                        <div className={styles['content-number']}>{dataRanking.number}</div>
                                    </div>
                                    <div
                                        className={styles['content-ber-chart']}
                                        style={{ width: `${(100 / MaxNumber()) * dataRanking.number}%` }}
                                    >
                                        <div className={styles['content-test']}></div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            )}
            {!dataRankings.isLoading ? (
                <div className={styles['notification']}>
                    {dataRankings?.isFailure?.message ? dataRankings?.isFailure?.message : ''}
                </div>
            ) : (
                ''
            )}
        </div>
    );
}

export default RankingChart;
