import React from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DataPieChart } from 'src/type/chart-data.type';
import { VictoryPie } from 'victory';

// import usePieChart from '../../hook/usePieChart';
import * as actions from '../../redux/actions';
import { keysState$, pieChartState$ } from '../../redux/selector';
import LoadingPieChart from '../Loading';
import styles from './PieChart.module.scss';

function PieChart() {
    const dispatch = useDispatch();
    const dataDevices = useSelector(pieChartState$);
    const dataDevice = dataDevices.data.data;
    const keys = useSelector(keysState$);

    const arrColor: string[] = ['#6abdb0', '#8c6edb'];
    useEffect(() => {
        dispatch(actions.getPieChart());
    }, [dispatch]);
    // const { fetchPieChartData, dataDevice, keys } = usePieChart()
    // useEffect(() => {
    //     fetchPieChartData()
    // }, [])
    function total() {
        const sum = keys.reduce((total, value) => {
            return total + dataDevice[value as keyof typeof dataDevice];
        }, 0);
        return sum;
    }
    function createDataChart() {
        const arrData: DataPieChart[] = [];
        keys.map((key) => {
            return arrData.push({ x: key, y: dataDevice[key as keyof typeof dataDevice] });
        });
        return arrData;
    }
    function factorial(math: Math, string: string, number: number): string {
        return (number ? factorial(math, string, number - 1) : '#') + string[math.floor(math.random() * string.length)];
    }
    function randomColor(): string[] {
        for (let i = 0; i < keys.length; i++) {
            arrColor.push(factorial(Math, '0123456789ABCDEF', 5));
        }
        return arrColor;
    }
    return (
        <div className={styles['pie-chart']}>
            <div className={styles['title']}>Device Type </div>
            {dataDevices.isLoading ? (
                <LoadingPieChart />
            ) : (
                <div className={styles['description']}>
                    <div className={styles['chart']}>
                        <svg width={300} height={300}>
                            <VictoryPie
                                colorScale={randomColor()}
                                standalone={false}
                                width={300}
                                height={300}
                                innerRadius={75}
                                data={createDataChart()}
                                labels={() => ``}
                            />
                        </svg>
                    </div>
                    <div className={styles['information']}>
                        {keys.map((key, index) => {
                            return (
                                <div key={index} className={styles['information-item']}>
                                    <div className={styles['wrap']}>
                                        <div className={styles['icon-name']}>
                                            <div
                                                className={styles['icon']}
                                                style={{ backgroundColor: arrColor[index] }}
                                            ></div>
                                            <span className={styles['name']}>{key}</span>
                                        </div>
                                        <div className={styles['count']}>
                                            {dataDevice[key as keyof typeof dataDevice]}
                                        </div>
                                    </div>
                                    <div className={styles['percent']}>
                                        {((dataDevice[key as keyof typeof dataDevice] / total()) * 100).toFixed(0)}%
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
            )}

            {!dataDevices.isLoading ? (
                <div className={styles['notification']}>
                    {dataDevices?.isFailure?.message ? dataDevices?.isFailure?.message : ''}
                </div>
            ) : (
                ''
            )}
        </div>
    );
}
export default PieChart;
