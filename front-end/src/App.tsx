import React from 'react';

import HeatChart from './components/DeviceByHour';
import PieChart from './components/PieChart';
import RankingChart from './components/Rankking';
const App: React.FC = () => {
    return (
        <div
            className="App"
            style={{ backgroundColor: '#e9e9e9', height: '100vh', width: '100vw', position: 'relative' }}
        >
            <PieChart />
            <RankingChart />
            <HeatChart />
        </div>
    );
};
export default App;
