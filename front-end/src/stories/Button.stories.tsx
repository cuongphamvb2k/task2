import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';

import { Button } from '../components/Button/Button';
export default {
    title: 'Button',
    component: Button,
    argTypes: {
        backgroundColor: { control: 'color' },
    },
} as ComponentMeta<typeof Button>;
const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;
export const Ok = Template.bind({});
Ok.args = {
    label: 'OK',
    type: 'Ok',
};
export const Cancel = Template.bind({});
Cancel.args = {
    label: 'Cancel',
    type: 'Cancel',
};