import axios from 'axios';
import { useState } from 'react';

function usePieChart() {
    const [dataDevice, setDataDevice] = useState({});
    const [keys, setKeys] = useState([]);
    const axiosClient = axios.create({
        baseURL: 'http://localhost:8000',
        headers: { 'content-type': 'application/json' },
    });

    const fetchPieChartData = async () => {
        const PieChartData = await axiosClient.get('/device_summary');
        setDataDevice(PieChartData.data);
        setKeys(Object.keys(PieChartData.data));
    };

    return { fetchPieChartData, dataDevice, keys };
}

export default usePieChart;
