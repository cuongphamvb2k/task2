export const INIT_STATE = {
    pieChart: {
        isLoading: false,
        data: [],
    },
};

export const INIT_STATE_RANKING = {
    rankingChart: {
        isLoading: false,
        data: [],
    },
};

export const INIT_STATE_HEATMAP = {
    heatMapChart: {
        isLoading: false,
        data: [],
    },
};
