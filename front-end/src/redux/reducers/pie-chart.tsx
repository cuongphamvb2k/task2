import { ActionPieChar } from 'src/type/chart-data.type';

import { FETCH_DEVICE_SUMMARY, FETCH_DEVICE_SUMMARY_FAILDED, FETCH_DEVICE_SUMMARY_SUCCESS } from '../actions/action';
import { INIT_STATE } from './constants';

export default function pieChartReducers(state = INIT_STATE.pieChart, action: ActionPieChar) {
    switch (action.type) {
        case FETCH_DEVICE_SUMMARY:
            return {
                ...state,
                isLoading: true,
            };
        case FETCH_DEVICE_SUMMARY_SUCCESS:
            return {
                ...state,
                isLoading: false,
                data: action.result,
            };
        case FETCH_DEVICE_SUMMARY_FAILDED:
            return {
                ...state,
                isLoading: false,
                isFailure: action.result,
            };
        default:
            return state;
    }
}
