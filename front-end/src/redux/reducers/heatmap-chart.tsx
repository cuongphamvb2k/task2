import { ActionHeatMapChar } from 'src/type/chart-data.type';

import { FETCH_HEATMAP_SUMMARY, FETCH_HEATMAP_SUMMARY_FAILDED, FETCH_HEATMAP_SUMMARY_SUCCESS } from '../actions/action';
import { INIT_STATE_HEATMAP } from './constants';

export default function heatMapChartReducers(state = INIT_STATE_HEATMAP.heatMapChart, action: ActionHeatMapChar) {
    switch (action.type) {
        case FETCH_HEATMAP_SUMMARY:
            return {
                ...state,
                isLoading: true,
            };
        case FETCH_HEATMAP_SUMMARY_SUCCESS:
            return {
                ...state,
                isLoading: false,
                data: action.result,
            };
        case FETCH_HEATMAP_SUMMARY_FAILDED:
            return {
                ...state,
                isLoading: false,
                isFailure: action.result,
            };
        default:
            return state;
    }
}
