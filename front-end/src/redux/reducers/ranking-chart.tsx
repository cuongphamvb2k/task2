import { ActionRankingChar } from 'src/type/chart-data.type';

import { FETCH_RANKING_SUMMARY, FETCH_RANKING_SUMMARY_FAILDED, FETCH_RANKING_SUMMARY_SUCCESS } from '../actions/action';
import { INIT_STATE_RANKING } from './constants';

export default function rankingChartReducers(state = INIT_STATE_RANKING.rankingChart, action: ActionRankingChar) {
    switch (action.type) {
        case FETCH_RANKING_SUMMARY:
            return {
                ...state,
                isLoading: true,
            };
        case FETCH_RANKING_SUMMARY_SUCCESS:
            return {
                ...state,
                isLoading: false,
                data: action.result,
            };
        case FETCH_RANKING_SUMMARY_FAILDED:
            return {
                ...state,
                isLoading: false,
                isFailure: action.result,
            };
        default:
            return state;
    }
}
