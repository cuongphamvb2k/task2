import { combineReducers } from 'redux';

import heatMapChart from './heatmap-chart';
import pieChart from './pie-chart';
import rankingChart from './ranking-chart';

export default combineReducers({
    pieChart,
    rankingChart,
    heatMapChart,
});
