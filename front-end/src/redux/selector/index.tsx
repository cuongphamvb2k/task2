import { State } from 'src/type/chart-data.type';

export const pieChartState$ = (state: State) => state.pieChart;

export const keysState$ = (state: State) => Object.keys(state.pieChart.data.data || {});

export const rankingChartState$ = (state: State) => state.rankingChart;

export const heatMapChartState$ = (state: State) => state.heatMapChart;
