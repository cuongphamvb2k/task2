import {
    AxiosError,
    AxiosResponseHeatMapChart,
    AxiosResponsePieChart,
    AxiosResponseRankingChart,
} from 'src/type/chart-data.type';

import {
    FETCH_DEVICE_SUMMARY,
    FETCH_DEVICE_SUMMARY_FAILDED,
    FETCH_DEVICE_SUMMARY_SUCCESS,
    FETCH_HEATMAP_SUMMARY,
    FETCH_HEATMAP_SUMMARY_FAILDED,
    FETCH_HEATMAP_SUMMARY_SUCCESS,
    FETCH_RANKING_SUMMARY,
    FETCH_RANKING_SUMMARY_FAILDED,
    FETCH_RANKING_SUMMARY_SUCCESS,
} from './action';

export const getPieChart = () => {
    return {
        type: FETCH_DEVICE_SUMMARY,
    };
};
export const getPieChartResult = (result: AxiosResponsePieChart | AxiosError, isSuccess: boolean) => ({
    type: isSuccess ? FETCH_DEVICE_SUMMARY_SUCCESS : FETCH_DEVICE_SUMMARY_FAILDED,
    result,
});

export const getRankingChartData = () => {
    return {
        type: FETCH_RANKING_SUMMARY,
    };
};
export const getRankingChartResult = (result: AxiosResponseRankingChart | AxiosError, isSuccess: boolean) => ({
    type: isSuccess ? FETCH_RANKING_SUMMARY_SUCCESS : FETCH_RANKING_SUMMARY_FAILDED,
    result,
});

export const getHeatMapChartData = () => {
    return {
        type: FETCH_HEATMAP_SUMMARY,
    };
};
export const getHeatMapChartResult = (result: AxiosResponseHeatMapChart | AxiosError, isSuccess: boolean) => ({
    type: isSuccess ? FETCH_HEATMAP_SUMMARY_SUCCESS : FETCH_HEATMAP_SUMMARY_FAILDED,
    result,
});
