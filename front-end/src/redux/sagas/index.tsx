import { call, put, takeLatest } from 'redux-saga/effects';

import * as api from '../../api';
import {
    AxiosResponseHeatMapChart,
    AxiosResponsePieChart,
    AxiosResponseRankingChart,
} from '../../type/chart-data.type';
import * as actions from '../actions';
import { FETCH_DEVICE_SUMMARY, FETCH_HEATMAP_SUMMARY, FETCH_RANKING_SUMMARY } from '../actions/action';

function* getPieChartSaga() {
    try {
        const isSuccess = true;
        const pieChartData: AxiosResponsePieChart = yield call(api.fetchPieChartData);
        yield put(actions.getPieChartResult(pieChartData, isSuccess));
    } catch (err: any) {
        const isSuccess = false;
        yield put(actions.getPieChartResult(err, isSuccess));
    }
}
function* getRankingChartSaga() {
    try {
        const isSuccess = true;
        const rankingChartData: AxiosResponseRankingChart = yield call(api.fetchRankingChartData);
        yield put(actions.getRankingChartResult(rankingChartData, isSuccess));
    } catch (err: any) {
        const isSuccess = false;
        yield put(actions.getRankingChartResult(err, isSuccess));
    }
}
function* getHeatMapChartSaga() {
    try {
        const isSuccess = true;
        const heatMapChartData: AxiosResponseHeatMapChart = yield call(api.fetchHeatMapChartData);
        yield put(actions.getHeatMapChartResult(heatMapChartData, isSuccess));
    } catch (err: any) {
        const isSuccess = false;
        yield put(actions.getHeatMapChartResult(err, isSuccess));
    }
}
function* mySaga() {
    yield takeLatest(FETCH_DEVICE_SUMMARY, getPieChartSaga);
    yield takeLatest(FETCH_RANKING_SUMMARY, getRankingChartSaga);
    yield takeLatest(FETCH_HEATMAP_SUMMARY, getHeatMapChartSaga);
}

export default mySaga;
