import { AxiosRequestConfig, AxiosResponse, AxiosResponseHeaders } from 'axios/index.d';

export interface AxiosResponsePieChart {
    config: AxiosRequestConfig;
    data: {
        ios: number;
        android: number;
    };
    headers: AxiosResponseHeaders;
    request?: any;
    status: number;
    statusText: string;
}

export interface AxiosResponseRankingChart {
    config: AxiosRequestConfig;
    data: [{ day: string; number: number }];
    headers: AxiosResponseHeaders;
    request?: any;
    status: number;
    statusText: string;
}

export interface AxiosResponseHeatMapChart {
    config: AxiosRequestConfig;
    data: [
        {
            day: string;
            hours: [
                {
                    time: string;
                    value: number;
                },
            ];
        },
    ];
    headers: AxiosResponseHeaders;
    request?: any;
    status: number;
    statusText: string;
}

export interface AxiosError {
    message?: string;
    code?: string;
    config?: AxiosRequestConfig;
    request?: any;
    response?: AxiosResponse;
    name?: string;
}

export interface ActionPieChar {
    type: string;
    result: AxiosResponsePieChart;
}

export interface ActionRankingChar {
    type: string;
    result: AxiosResponseRankingChart;
}

export interface ActionHeatMapChar {
    type: string;
    result: AxiosResponseHeatMapChart;
}

export interface DataPieChart {
    x: string;
    y: number;
}

export interface ArrTotalDay {
    day: string;
    total: number;
}

export interface Hours {
    time: string;
    value: number;
}

export interface State {
    pieChart: {
        isLoading: boolean;
        data: AxiosResponsePieChart;
        isFailure: AxiosError;
    };
    rankingChart: {
        isLoading: boolean;
        data: AxiosResponseRankingChart;
        isFailure: AxiosError;
    };
    heatMapChart: {
        isLoading: boolean;
        data: AxiosResponseHeatMapChart;
        isFailure: AxiosError;
    };
}
